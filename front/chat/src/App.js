import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './pages/home';
import Login from './pages/login';
import Chat from './pages/chat';
import './App.css';
import { useEffect, useState } from 'react';
import Signup from './pages/signup';


function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [username, setUsername] = useState("");

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("chatUser"))
    if (!user || !user.access_token) {
      setLoggedIn(false)
      return;
    }
    else {
      setUsername(user.username);
      setLoggedIn(true);
    }
}, [loggedIn])

  return (
    <div className="App">
       <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home username={username} loggedIn={loggedIn} setLoggedIn={setLoggedIn}/>} />
          <Route path="/login" element={<Login setLoggedIn={setLoggedIn} setUsername={setUsername} />} />
          <Route path="/signup" element={<Signup setLoggedIn={setLoggedIn} setUsername={setUsername} />} />
          <Route path="/chat" element={<Chat username={username} loggedIn={loggedIn} setLoggedIn={setLoggedIn} />} />
        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;
