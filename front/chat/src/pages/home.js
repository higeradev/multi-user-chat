import React from "react";
import { useNavigate } from "react-router-dom";

const Home = (props) => {
  const { loggedIn, username, setLoggedIn } = props;
  const navigate = useNavigate();

  const onButtonClick = () => {
    if (loggedIn) {
      localStorage.removeItem("chatUser");
      setLoggedIn(false);
    } else {
      navigate("/login");
    }
  };

  const onCreateUserButtonClick = () => {
    navigate("/signup");
  };

  return (
    <div className="mainContainer">
      <div className={"titleContainer"}>
        <div>Welcome to Chat App!</div>
      </div>
      {loggedIn ? (
        <div className={"username"}>Your username is {username}</div>
      ) : (
        <div />
      )}
      <div className={"buttonContainer"}>
        <input
          className={"inputButton"}
          type="button"
          onClick={onCreateUserButtonClick}
          value="Create user"
        />
        <input
          className={"inputButton"}
          type="button"
          onClick={onButtonClick}
          value={loggedIn ? "Log out" : "Log in"}
        />
        {loggedIn ? (
          <input
            className={"inputButton"}
            type="button"
            onClick={() => navigate("/chat")}
            value="Go to chat"
          />
        ) : null}
      </div>
    </div>
  );
};

export default Home;
