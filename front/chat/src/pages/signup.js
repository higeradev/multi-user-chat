import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { AppAPIUrl } from "../utils/AppConfig";
import axios from "axios";

const USER_EXIST_ERR = "User with such username already exists!";
const Signup = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [isUserExist, setIsUserExist] = useState(false);

  const navigate = useNavigate();

  async function signUp() {
    const data = {
        username,
        password,
      };

    try {
      const response = await axios.post(`${AppAPIUrl}/users`, data);
      if (response?.status === 200) {
        navigate("/login");
      } else if (response?.status === 403) {
        setIsUserExist(true);
      }
      return response;
    } catch (e) {
      return e;
    }
  }

  const onButtonClick = () => {
    setUsernameError("");
    setPasswordError("");

    if ("" === username) {
      setUsernameError("Please enter your username");
      return;
    }

    if (username.length < 4) {
      setPasswordError("The username must contain from 4 to 15 characters");
      return;
    }

    if ("" === password) {
      setPasswordError("Please enter a password");
      return;
    }

    if (password.length < 4) {
      setPasswordError("The password must contain from 4 to 15 characters");
      return;
    }

    signUp();
  };

  return (
    <div className={"mainContainer"}>
      <div className={"titleContainer"}>
        <div>Create user</div>
      </div>
      <br />
      <div className={"alert"}>{isUserExist ? USER_EXIST_ERR : null}</div>
      <div className={"inputContainer"}>
        <input
          value={username}
          placeholder="Enter your username here"
          onChange={(ev) => setUsername(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{usernameError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          value={password}
          placeholder="Enter your password here"
          onChange={(ev) => setPassword(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{passwordError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          className={"inputButton"}
          type="button"
          onClick={onButtonClick}
          value={"Log in"}
        />
      </div>
    </div>
  );
};

export default Signup;
