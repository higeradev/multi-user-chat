import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { AppAPIUrl } from "../utils/AppConfig";

const Chat = (props) => {
  const { username } = props;
  const [message, setMessage] = useState("");
  const [isMessageSent, setIsMessageSent] = useState(false);
  const [messageList, setMessageList] = useState([]);


  const navigate = useNavigate();

  async function onMessageSend() {
    
    const token = JSON.parse(localStorage.getItem("chatUser")).access_token;
    const headers = {
      Authorization: "Bearer " + token,
    };
    const data = {
      username,
      message,
    };

    try {
      const response = await axios.post(`${AppAPIUrl}/chat`, data, { headers });
      if (response?.status === 200) {
        setIsMessageSent(true);
      }
      setMessage("");
      return response;
    } catch (e) {
      return e;
    }
  }

  async function getMessages() {
    const token = JSON.parse(localStorage.getItem("chatUser")).access_token;
    const headers = {
      Authorization: "Bearer " + token,
    };
    try {
      const response = await axios.get(`${AppAPIUrl}/chat`, { headers });
      if (response?.status === 200) {
        setMessageList(response.data);
      }
      return response;
    } catch (e) {
      if (e.response.status === 403) {
        localStorage.removeItem("chatUser");
        navigate("/");
      }
      return e;
    }
  }
  
  useEffect(() => {
    let interval = setInterval(() => {
      getMessages();
    }, 2000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    if (isMessageSent) {
      getMessages(); 
      setIsMessageSent(false);
    }
     
  }, [isMessageSent]);

  return (
    <div className="mainContainer">
      <a href="/">Go to Home</a>
      <div className="titleContainer">
        <div>Chat</div>
      </div>
      <div className="chatWrapper">
        {messageList.length > 0 ? (messageList.map((d, index) => (
            <div key={index} className="messageBox">
              <div className="messsageUsername">{d.username}</div>
              <div className="messageText">{d.message}</div>
            </div>
        ))
        ) : "No messages yet"
      }
      </div>
      <div>
        <div className="inputContainer">
          <input
            value={message}
            placeholder="Enter message"
            onChange={(ev) => setMessage(ev.target.value)}
            className="inputBox"
          />
        </div>
        <div className={"inputContainer"}>
          <input
            className={"inputButton"}
            type="button"
            onClick={onMessageSend}
            value={"Send"}
          />
        </div>
      </div>
    </div>
  );
};

export default Chat;
