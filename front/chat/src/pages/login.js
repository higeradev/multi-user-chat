import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { AppAPIUrl } from "../utils/AppConfig";
import axios from "axios";

const Login = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const navigate = useNavigate();

  async function logIn() {
    try {
      const response = await axios.get(
        `${AppAPIUrl}/login?username=${username}&password=${password}`
      );
      if (response?.status === 200) {
        localStorage.setItem("chatUser", JSON.stringify({ username, access_token: response.headers["access_token"], refresh_token: response.headers["refresh_token"]}));
        props.setLoggedIn(true);
        navigate("/chat");
      }
      return response;
    } catch (e) {
      if (e.response.status === 401) {
        window.alert("Wrong username or password!");
      }
      return e;
    }
  }

  const onButtonClick = () => {
    setUsernameError("");
    setPasswordError("");

    if ("" === username) {
      setUsernameError("Please enter your username");
      return;
    }

    if (username.length < 4) {
      setPasswordError("The username must contain from 4 to 15 characters");
      return;
    }

    if ("" === password) {
      setPasswordError("Please enter a password");
      return;
    }

    if (password.length < 4) {
      setPasswordError("The password must contain from 4 to 15 characters");
      return;
    }

    logIn();
  };

  return (
    <div className={"mainContainer"}>
      <div className={"titleContainer"}>
        <div>Login</div>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          value={username}
          placeholder="Enter your email"
          onChange={(ev) => setUsername(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{usernameError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          value={password}
          placeholder="Enter your password"
          onChange={(ev) => setPassword(ev.target.value)}
          className={"inputBox"}
        />
        <label className="errorLabel">{passwordError}</label>
      </div>
      <br />
      <div className={"inputContainer"}>
        <input
          className={"inputButton"}
          type="button"
          onClick={onButtonClick}
          value={"Log in"}
        />
      </div>
    </div>
  );
};

export default Login;
