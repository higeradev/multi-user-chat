package com.chat.DTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {
    @NotBlank
    private String username;

    @NotBlank
    @Size(min = 2, max = 100, message = "Length must be ≥ 2 and ≤ 100")
    private String message;
}
