package com.chat.DTO;

import com.chat.models.User;
import lombok.*;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDTO {
    private String username;

    public static GetUserDTO map(User user)
    {
        return new GetUserDTO(user.getUsername());
    }
}
