package com.chat.DTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    @NotBlank
    @Size(min = 4, max = 15, message = "Length must be ≥ 4 and ≤ 15")
    private String username;

    @NotBlank
    @Size(min = 4, max = 15, message = "Length must be ≥ 4 and ≤ 15")
    private String password;

}
