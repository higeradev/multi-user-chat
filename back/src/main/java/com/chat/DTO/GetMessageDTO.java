package com.chat.DTO;

import com.chat.models.Message;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetMessageDTO {
    private String username;
    private String message;

    public static GetMessageDTO map(Message message) {
        return new GetMessageDTO(message.getUser().getUsername(), message.getMessage());
    }
}
