package com.chat.repositories;

import com.chat.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<com.chat.models.User, Long> {
    Optional<User> findByUsername(String username);
    boolean existsByUsername(String username);
}
