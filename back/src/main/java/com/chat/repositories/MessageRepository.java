package com.chat.repositories;

import com.chat.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
