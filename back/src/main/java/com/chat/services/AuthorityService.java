package com.chat.services;


import com.chat.models.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAllByAuthority(String authority);
}
