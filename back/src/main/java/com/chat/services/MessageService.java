package com.chat.services;

import com.chat.DTO.GetMessageDTO;
import com.chat.DTO.MessageDTO;
import com.chat.models.Message;

import java.util.List;

public interface MessageService {
    void save(MessageDTO messageDTO);
    Message findById(Long messageId);
    List<GetMessageDTO> getAll();
}
