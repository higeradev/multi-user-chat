package com.chat.services.impl;

import com.chat.DTO.GetMessageDTO;
import com.chat.DTO.MessageDTO;
import com.chat.models.Message;
import com.chat.models.User;
import com.chat.repositories.MessageRepository;
import com.chat.services.MessageService;
import com.chat.services.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository repository;
    private final UserService userService;
    public MessageServiceImpl(MessageRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    public void save(MessageDTO messageDTO) {
        User user = userService.findByUsername(messageDTO.getUsername());
        Message message = Message.builder().user(user).message(messageDTO.getMessage()).build();
        userService.addMessage(user, message);

        repository.save(message);
    }

    @Override
    public Message findById(Long messageId) {
        return null;
    }

    @Override
    public List<GetMessageDTO> getAll() {
        List<Message> messages = repository.findAll();
        return messages.stream().map(GetMessageDTO::map).collect(Collectors.toList());
    }
}
