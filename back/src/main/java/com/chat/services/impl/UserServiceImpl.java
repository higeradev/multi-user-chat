package com.chat.services.impl;

import com.chat.DTO.UserDTO;
import com.chat.models.Message;
import com.chat.models.User;
import com.chat.repositories.UserRepository;
import com.chat.services.AuthorityService;
import com.chat.services.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder encoder;
    private final AuthorityService service;

    public UserServiceImpl(UserRepository repository, PasswordEncoder encoder, AuthorityService service) {
        this.repository = repository;
        this.encoder = encoder;
        this.service = service;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = repository.findByUsername(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }

        return user.get();
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User findById(Long id) {
        Optional<User> user = repository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        throw new UsernameNotFoundException("User with id %s doesn't exists!".formatted(id));
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> user = repository.findByUsername(username);

        if (user.isEmpty())
        {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }

        return user.get();
    }

    @Override
    public void create(UserDTO userDTO) {
        String username = userDTO.getUsername();

        if (exists(username))
        {
            throw new UsernameNotFoundException("User with username %s already exists!".formatted(username));
        }

        User user = User
                .builder()
                .username(username)
                .password(encoder.encode(userDTO.getPassword()))
                .messages(new ArrayList<>())
                .authorities(service.findAllByAuthority("USER"))
                .build();

        repository.save(user);
    }

    @Override
    public boolean exists(String username) {
        return repository.existsByUsername(username);
    }

    @Override
    public void addMessage(User user, Message message) {

    }

}
