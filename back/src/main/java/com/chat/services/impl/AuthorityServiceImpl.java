package com.chat.services.impl;

import com.chat.models.Authority;
import com.chat.repositories.AuthorityRepository;
import com.chat.services.AuthorityService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository repository;

    public AuthorityServiceImpl(AuthorityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Authority> findAllByAuthority(String authority) {
        return repository.findAllByAuthority(authority);
    }
}
