package com.chat.services;

import com.chat.DTO.UserDTO;
import com.chat.models.Message;
import com.chat.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<User> findAll();
    User findById(Long id);
    User findByUsername(String username);
    void create(UserDTO userDTO);
    boolean exists(String username);
    void addMessage(User user, Message message);
}
