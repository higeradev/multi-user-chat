package com.chat.controllers;

import com.chat.DTO.GetMessageDTO;
import com.chat.DTO.MessageDTO;
import com.chat.services.MessageService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/chat")
public class MessageController {
    private final MessageService service;

    public MessageController(MessageService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody MessageDTO messageDTO) {
        service.save(messageDTO);
    }

    @GetMapping
    public ResponseEntity<List<GetMessageDTO>> getAll() {
        try {
            List<GetMessageDTO> messages = service.getAll();

            return ResponseEntity
                    .ok()
                    .body(messages);
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }
}
