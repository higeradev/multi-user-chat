package com.chat.controllers;


import com.chat.DTO.GetUserDTO;
import com.chat.DTO.UserDTO;
import com.chat.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<String> create(@RequestBody @Valid UserDTO userDTO) {
        service.create(userDTO);
        return ResponseEntity
                .status(200)
                .body("User created");
    }

    @GetMapping("/all")
    public List<GetUserDTO> get() {
        return service
                .findAll()
                .stream()
                .map(GetUserDTO::map)
                .collect(Collectors.toList());
    }

}
